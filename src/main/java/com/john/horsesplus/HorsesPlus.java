package com.john.horsesplus;

import com.john.horsesplus.listener.HorseMovementListener;
import net.minecraft.client.renderer.entity.RenderAbstractHorse;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.FMLModContainer;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.lang.reflect.Field;
import java.util.Map;

@Mod(modid = HorsesPlus.MOD_ID, name = HorsesPlus.MOD_NAME, version = HorsesPlus.MOD_VERSION)
public class HorsesPlus {
    // specific mod details
    public static final String MOD_ID = "horsesplus";
    public static final String MOD_NAME = "HorsesPlus";
    public static final String MOD_VERSION = "1.0.9";

    @Mod.EventHandler
    public void preInit(final FMLPreInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(new HorseMovementListener());
    }
}
