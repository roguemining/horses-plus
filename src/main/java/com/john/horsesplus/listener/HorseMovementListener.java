package com.john.horsesplus.listener;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.passive.AbstractHorse;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.lang.reflect.Field;

public final class HorseMovementListener {
    private Vec3d lastPosition = null;
    private double lastTravelDistance = 0.0d;

    @SubscribeEvent
    public void onLivingUpdate(final LivingEvent.LivingUpdateEvent event) {
        final EntityPlayerSP player = Minecraft.getMinecraft().player;
        if (player == null || player.getRidingEntity() == null || !(event.getEntity() instanceof AbstractHorse)) {
            lastTravelDistance = 0.0f;
            return;
        }

        if (event.getEntity().equals(player.getRidingEntity())) {
            final EntityHorse horse = (EntityHorse) event.getEntity();

            final IAttribute jumpStrength = getAttributeFromClass(AbstractHorse.class, horse, "horse.jumpStrength");
            final IAttribute movementSpeed = SharedMonsterAttributes.MOVEMENT_SPEED;
            if (jumpStrength == null || movementSpeed == null)
                return;

            horse.getEntityAttribute(jumpStrength).setBaseValue(1.0d);
            horse.getEntityAttribute(movementSpeed).setBaseValue(0.3375D);
        }
    }

    @SubscribeEvent
    public void onGui(final RenderGameOverlayEvent.Text text) {
        final EntityPlayerSP player = Minecraft.getMinecraft().player;
        if (player == null || player.getRidingEntity() == null)
            return;

        final Entity riding = player.getRidingEntity();
        if (riding instanceof AbstractHorse) {
            // get difference in positions
            final double diffX = riding.posX - riding.lastTickPosX;
            final double diffZ = riding.posZ - riding.lastTickPosZ;

            // setup the last travel distance from the previous tick data
            lastTravelDistance = Math.sqrt(diffX * diffX + diffZ * diffZ);

            // calculate the tick travel time (20 ticks per second, based on singular tick data)
            text.getLeft().add("blocks per minute: " + Math.floor((lastTravelDistance * 20) * 60));
        }
    }

    private <T extends Entity> IAttribute getAttributeFromClass(final Class<? extends T> entityClass, final T entity, final String attributeName) {
        if (attributeName.isEmpty())
            return null;

        try {
            for (final Field field : entityClass.getDeclaredFields()) {
                if (field.getType().equals(IAttribute.class)) {
                    field.setAccessible(true);
                    final IAttribute attribute = (IAttribute) field.get(entity);
                    if (attribute.getName().equalsIgnoreCase(attributeName))
                        return attribute;
                }
            }
        } catch (Exception ignored) { }

        return null;
    }
}
